#!groovy 
/* 
    
     Definicion de constantes 

*/
def ip = [DEV:'10.11.52.131' , SIT: '10.11.44.95, 10.11.44.96' , UAT:'10.11.37.125, 10.11.37.126' , STG: '10.11.192.38,10.11.192.39,10.11.192.40,10.11.192.41' ]
def ipsRestart = ip."${AMBIENTE}".toString().trim().split(",")
node('smp') {
 stage('REINICIANDO NODO DE WAS') {
   println("AMBIENTE: "+ "${AMBIENTE}")
   for(int i : ipsRestart)
   sh "ssh -o 'StrictHostKeyChecking no' devops@"+ i +" echo 'Abriendo conexion' "
   sh "ssh -o 'StrictHostKeyChecking no' devops@" + i + " systemctl restart service websphere"
 }
}