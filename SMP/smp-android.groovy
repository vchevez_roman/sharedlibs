#!groovy 
/* 
    
     Definicion de constantes 

*/
def env = params.ENTORNO
def STAGES = params.STAGES
def server = Artifactory.server 'Arti'
def buildInfo
// Inicio de Pipeline 
node("smp") {
   stage('Checkout Repo') {
     descargaRepo("${BRANCH}", "${REPO}")
  }

  stage('Pruebas Unitarias') {
    sh "./gradlew app:generateCorporateDebugSources --stacktrace"
  }

  stage('Sonar ') {
    if ( STAGES ==~ /.*SONAR.*/ && !(env == "UAT" || env == "STG")) { 
      
    }
  }
  
  stage('Generacion y entrega de Artefactos') {
    if ( STAGES ==~ /.*ARTIFACTORY.*/ && !(env == "UAT" || env == "STG") ) {
      sh "export JAVA_HOME=/usr/java/jdk1.7.0_80"
        rtMaven = Artifactory.newMavenBuild()
        rtMaven.tool = 'MAVEN_3.5.4' 
        rtMaven.deployer releaseRepo: 'smp-stable', snapshotRepo: 'smp-unstable', server: server
        rtMaven.resolver releaseRepo: 'libs-release', snapshotRepo: 'libs-snapshot', server: server
        rtMaven.deployer.deployArtifacts = true 
        rtMaven.opts= '-DskipTests=true -Dproject.build.sourceEncoding=UTF-8 -Dproject.reporting.outputEncoding=UTF-8 -Dfile.encoding=UTF-8'
        rtMaven.run pom: '$WORKSPACE/${RUTA_FUENTES}/pom.xml', goals: 'clean package -U  -DuniqueVersion=false', buildInfo: buildInfo
    }
  }

  stage('Generando dependencias') {
    if (STAGES ==~ /.*DEPLOY.*/ ) {  
       dir("$WORKSPACE/${RUTA_FUENTES}/${RUTA_EAR}") {
       sh "rm -rf /target/dependency/*"
       }
       dir("$WORKSPACE/${RUTA_FUENTES}") {
          sh "mvn dependency:copy-dependencies -DincludeScope=provided -DoutputDirectory=$WORKSPACE/${fuentesPath}/target/dependency/"
       }
    }
  
  }
  stage('Descargando Ansible Playbook') {
    if (STAGES ==~ /.*DEPLOY.*/) {
       dir("$WORKSPACE/ansible") {
         descargaRepo("${ansibleRepoBranch}", "${ansibleRepo}")  
          }  }
  }

  stage('Descargando Ansible Tasks') {
    if (STAGES ==~ /.*DEPLOY.*/) {
    dir("$WORKSPACE/ansible/ansible/shared-tasks") {
      descargaRepo("${ansibleTasksBranch}", "${ansibleTasksRepo}")
    }
  }
  }

  stage('Desplegando Aplicacion') {
    if (STAGES ==~ /.*DEPLOY.*/) {
    dir("$WORKSPACE/ansible/ansible/") {

      def nombreArtefacto = obtenerNombreArtefacto("$WORKSPACE/${RUTA_FUENTES}","${RUTA_EAR}")
      def extraVars = "deploy=back sharedLib=${LibreriaCompartida} sharedlibPath=${LibreriaCompartidaPath} artifact_name=${nombreArtefacto}  loader=${isolated} env=${env} "
      def dependenciesPath="dependency_src=$WORKSPACE/${fuentesPath}/target/dependency/*.jar"
      def environment_name= "${env}".toLowerCase()
      def artifactSrc = "artifact_src="+ obtenerDireccionDescarga("$WORKSPACE/${RUTA_FUENTES}/${RUTA_EAR}")

     sh "chmod 0400 keys/devops_rsa"
     sh "ansible-playbook -i inventory/smp.${environment_name} --extra-vars '${extraVars} ${artifactSrc} app_name=${Nombre_App} ${dependenciesPath} context=${CONTEXT}' deploy.yml -vvvv"
    }
  }
  }
  
}


/* 
    
     Definicion de metodos 

*/
def descargaRepo(def branch, def repo) {
   println("Descargando Repositorio: " + "${repo}")
   git branch: "${branch}",
   credentialsId: 'idBitbucketJenkinsIBK',
   url: "${repo}"
}
