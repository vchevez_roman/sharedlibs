#!groovy 
/* 
    
     Definicion de constantes 

*/
def env = params.ENTORNO
def STAGES = params.STAGES
//def raiz = (params.RUTA_FUENTES != "") ? params.BRANCH : false
def encoding = "-Dproject.build.sourceEncoding=UTF-8  -Dproject.reporting.outputEncoding=UTF-8  -Dfile.encoding=UTF-8 "
def ansibleRepo= "https://bitbucket.org/ibkteam/smp-devops/"
def ansibleRepoBranch= "feature/deployment"
def ansibleTasksRepo= "https://bitbucket.org/ibkteam/ansible-tasks/"
def ansibleTasksBranch= "feature/refactor"
def fuentesPath= ("${RUTA_FUENTES}" != null && "${RUTA_EAR}" != null) ? "${RUTA_FUENTES}/${RUTA_EAR}" : ""
def rtMaven = Artifactory.newMavenBuild()
def server = Artifactory.server 'Artifactory'
def buildInfo
// Inicio de Pipeline 
node("smp") {

  environment {
    JAVA_HOME= '/usr/java/jdk1.7.0_80'
  }
  stage('Limpiando Workspace') {
        dir("$WORKSPACE") {
          deleteDir()
  }
  }
   stage('Checkout Repo') {
     descargaRepo("${BRANCH}", "${REPO}")
  }

  stage('Sonar ') {
    if ( STAGES ==~ /.*SONAR.*/ && !(env == "UAT" || env == "STG")) { 
      dir("$WORKSPACE/${RUTA_FUENTES}") {
           sh "mvn clean install org.sonarsource.scanner.maven:sonar-maven-plugin:3.6.0.1398:sonar -DskipTests=true -Dsonar.host.url=http://10.11.44.60/sonarqube ${encoding}" 
      }
       
    }
  }
  
  stage('Generacion y entrega de Artefactos') {
    if ( STAGES ==~ /.*ARTIFACTORY.*/ && !(env == "UAT" || env == "STG") ) {
      sh "export JAVA_HOME=/usr/java/jdk1.7.0_80"
        rtMaven = Artifactory.newMavenBuild()
        rtMaven.tool = 'MAVEN_3.5.4' 
        rtMaven.deployer releaseRepo: 'smp-stable', snapshotRepo: 'smp-unstable', server: server
        rtMaven.resolver releaseRepo: 'libs-release', snapshotRepo: 'libs-snapshot', server: server
        rtMaven.deployer.deployArtifacts = true 
        rtMaven.opts= '-DskipTests=true -Dproject.build.sourceEncoding=UTF-8  -Dproject.reporting.outputEncoding=UTF-8  -Dfile.encoding=UTF-8'
        rtMaven.run pom: '$WORKSPACE/${RUTA_FUENTES}/pom.xml', goals: 'clean package -U  -DuniqueVersion=false', buildInfo: buildInfo
    }
  }

  stage('Generando dependencias') {
    if (STAGES ==~ /.*DEPLOY.*/ ) {  
       dir("$WORKSPACE/${RUTA_FUENTES}/") {
       sh "rm -rf /target/dependency/*"
       }
       dir("$WORKSPACE/${RUTA_FUENTES}") {
          sh "mvn dependency:copy-dependencies -DincludeScope=provided -DoutputDirectory=$WORKSPACE/${fuentesPath}/target/dependency/ -U"
       }
    }
  
  }
  stage('Descargando Ansible Playbook') {
    if (STAGES ==~ /.*DEPLOY.*/) {
       dir("$WORKSPACE/ansible") {
         descargaRepo("${ansibleRepoBranch}", "${ansibleRepo}")  
          }  }
  }

  stage('Descargando Ansible Tasks') {
    if (STAGES ==~ /.*DEPLOY.*/) {
    dir("$WORKSPACE/ansible/ansible/shared-tasks") {
      descargaRepo("${ansibleTasksBranch}", "${ansibleTasksRepo}")
    }
  }
  }

  stage('Desplegando Aplicacion') {
    if (STAGES ==~ /.*DEPLOY.*/) {
    dir("$WORKSPACE/ansible/ansible/") {

      def nombreArtefacto = obtenerNombreArtefacto("$WORKSPACE/${RUTA_FUENTES}","${RUTA_EAR}")
      def extraVars = "deploy=back sharedLib=${LibreriaCompartida} sharedlibPath=${LibreriaCompartidaPath} artifact_name=${nombreArtefacto}  loader=${isolated} env=${env} "
      def dependenciesPath="dependency_src=$WORKSPACE/${fuentesPath}/target/dependency/*.jar"
      def environment_name= "${env}".toLowerCase()
      def artifactSrc = "artifact_src="+ obtenerDireccionDescarga("$WORKSPACE/${RUTA_FUENTES}/${RUTA_EAR}")

     sh "chmod 0400 keys/devops_rsa"
     sh "ansible-playbook -i inventory/smp.${environment_name} --extra-vars '${extraVars} ${artifactSrc} app_name=${Nombre_App} ${dependenciesPath} context=${CONTEXT}' deploy.yml -vvvv"
    }
  }
  }
  
}


/* 
    
     Definicion de metodos 

*/
def descargaRepo(def branch, def repo) {
   println("Descargando Repositorio: " + "${repo}")
   git branch: "${branch}",
   credentialsId: 'idBitbucketJenkinsIBK',
   url: "${repo}"
}

String obtenerNombreArtefacto(String ws, String nombreApp) {

     String nombreArtefacto = " "
     dir("${ws}/${nombreApp}") {
       nombreArtefacto = "${nombreApp}" + "-" + readMavenPom().getVersion() + ".ear"
     }
    return nombreArtefacto
}

String obtenerDireccionDescarga(String ws) {
  String direccionDescarga = " "
  String groupId= " "

  dir("${ws}") {
    groupId= readMavenPom().getGroupId()

   def newGroup= groupId.replace(".","/")

    direccionDescarga = newGroup + "/" + readMavenPom().getArtifactId() + "/" + readMavenPom().getVersion()
  }
  return direccionDescarga
}
